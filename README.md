Naive Bayes
HAM precision 966/966 = 1.0
HAM recall 966/1000 = 0.966
HAM F-score = 0.982706002034588
SPAM precision 363/397 = 0.9143576826196473
SPAM recall 363/363 = 1.0
SPAM F-score = 0.9552631578947368

POS precision 49/50 = 0.98
POS recall 49/62 = 0.7903225806451613
POS F-score = 0.8749999999999999
NEG precision 63/76 = 0.8289473684210527
NEG recall 63/64 = 0.984375
NEG F-score = 0.9

SVM
Precision of HAM = 990/1040 = 0.9519230769230769
Recall of HAM = 990/1000 = 0.99
F-score of HAM = 0.9705882352941176
Precision of SPAM = 313/323 = 0.9690402476780186
Recall of SPAM = 313/363 = 0.8622589531680441
F-score of SPAM = 0.9125364431486881

Precision of NEG = 63/76 = 0.8289473684210527
Recall of NEG = 63/64 = 0.984375
F-score of NEG = 0.9
Precision of POS = 49/50 = 0.98
Recall of POS = 49/62 = 0.7903225806451613
F-score of POS = 0.8749999999999999

MegaM
Precision of HAM = 993/1061 = 0.9359095193213949
Recall of HAM = 993/1000 = 0.993
F-score of HAM = 0.9636098981077147
Precision of SPAM = 295/302 = 0.9768211920529801
Recall of SPAM = 295/363 = 0.8126721763085399
F-score of SPAM = 0.8872180451127819

Precision of NEG = 42/70 = 0.6
Recall of NEG = 42/64 = 0.65625
F-score of NEG = 0.626865671641791
Precision of POS = 34/56 = 0.6071428571428571
Recall of POS = 34/62 = 0.5483870967741935
F-score of POS = 0.5762711864406779

When only 10% of the training data is used to train the model, the F-score value was reduced. The new results are shown below
NB
SPAM precision 351/436 = 0.805045871559633
SPAM recall 351/363 = 0.9669421487603306
SPAM F-score = 0.8785982478097623
HAM precision 915/927 = 0.9870550161812298
HAM recall 915/1000 = 0.915
HAM F-score = 0.9496626881162429

NEG precision 57/77 = 0.7402597402597403
NEG recall 57/64 = 0.890625
NEG F-score = 0.8085106382978724
POS precision 42/49 = 0.8571428571428571
POS recall 42/62 = 0.6774193548387096
POS F-score = 0.7567567567567567

SVM
Precision of HAM = 155/179 = 0.8659217877094972
Recall of HAM = 155/1000 = 0.155
F-score of HAM = 0.26293469041560646
Precision of SPAM = 339/1184 = 0.28631756756756754
Recall of SPAM = 339/363 = 0.9338842975206612
F-score of SPAM = 0.4382676147382029

Precision of NEG = 59/83 = 0.7108433734939759
Recall of NEG = 59/64 = 0.921875
F-score of NEG = 0.8027210884353742
Precision of POS = 38/43 = 0.8837209302325582
Recall of POS = 38/62 = 0.6129032258064516
F-score of POS = 0.7238095238095238

MegaM
Precision of HAM = 701/773 = 0.906856403622251
Recall of HAM = 701/1000 = 0.701
F-score of HAM = 0.7907501410039481
Precision of SPAM = 291/590 = 0.49322033898305084
Recall of SPAM = 291/363 = 0.8016528925619835
F-score of SPAM = 0.6107030430220357

The reason for this degradation of F-score is that with reduced training data a lot of features are not part of the model and when they are encountered during test, the actual probability values cannot be estimated, applying techniques like add one smoothing at this point would result in an approximation of real world probabilities and incorrect results.